State management - from From Ken C. Dodds

https://kentcdodds.com/blog/how-to-use-react-context-effectively 
Note, please do read Application State Management with React and follow the advice that you shouldn't be reaching for context to solve every state sharing problem that crosses your desk. But when you do need to reach for context, hopefully this blog post will help you know how to do so effectively. Also, remember that context does NOT have to be global to the whole app, but can be applied to one part of your tree and you can (and probably should) have multiple logically separated contexts in your app.

https://kentcdodds.com/blog/how-to-optimize-your-context-value
https://kentcdodds.com/blog/application-state-management-with-react
https://kentcdodds.com/blog/fix-the-slow-render-before-you-fix-the-re-render
https://hswolff.com/blog/how-to-usecontext-with-usereducer/
https://react-redux.js.org/using-react-redux/connect-mapstate#mapstatetoprops-and-performance

Best pratices and tips - from From Ken C. Dodds

https://kentcdodds.com/blog/prop-drilling
https://kentcdodds.com/blog/colocation
https://kentcdodds.com/blog/state-colocation-will-make-your-react-app-faster
https://kentcdodds.com/blog/when-to-break-up-a-component-into-multiple-components

