import React, { useReducer, useState } from 'react';

/**
 * State management with useReducer
 * BAD: Children components are always updated when state changes because of drilling props
 * FIX: we memoize components with React.memo
 */


interface Counter {
  count: number;
  random: number;
}

type CounterActions = {
  type: string;
  payload?: Counter;
}

function countReducer(state: Counter, action: CounterActions) {
  switch (action.type) {
    case 'decrement': return ({...state, count: state.count - 1});
    case 'increment': return ({...state, count: state.count + 1});
    case 'random': return ({...state, random: Math.random()});
    default: throw new Error(`Unhandled action: ${action.type}`)
  }
}

/**
 * Main Smart Component
 * BAD: always rendered when state changes
 */

// const initialValue: Counter = { value: 1 };

export default function Demo3useReducer() {
  console.log('App: render')
  const [state, dispatch] = useReducer(countReducer, { count: 1, random: 0});

  return (
    <div className="comp">
      <h1>Demo Hooks: useReducer</h1>
      <button onClick={() => dispatch({ type: 'decrement' })}>-</button>
      <button onClick={() => dispatch({ type: 'increment' })}>+</button>
      <button onClick={() => dispatch({ type: 'random' })}>Random</button>
      <Dashboard count={state.count} random={state.random} />
    </div>
  );
}

// Middle Component
// BAD: always rendered when state changes since it receives them as props

interface DashboardProps {
  count: number, random: number
}

const Dashboard: React.FC<DashboardProps> = React.memo(props => {
  console.log('Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel value={props.count} />
    <RandomPanel value={props.random} />
  </div>
})

// Child Component
// BAD: rendered even when Random is updated and it's not necessary
function CounterPanel (props: { value: number }) {
  console.log('CounterPanel: render')
  return <div className="comp">
    Count: {props.value}
  </div>
}

// Child Component
// BAD: rendered even when Count is updated and it's not necessary
// FIXED: use React.memo to memoize it
const RandomPanel: React.FC<{ value: number }> = React.memo((props) => {
  console.log('Random Panel: render')
  return <div className="comp">
    Random Value: {props.value}
  </div>
})


