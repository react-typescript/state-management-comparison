import React, { useState } from 'react';

/**
 * State management with useState and drilling props
 * BAD: Children components are always updated when state changes because of drilling props
 * FIX: we memoize components with React.memo
 */


/**
 * Main Smart Component
 * BAD: always rendered when state changes
 */
export default function Demo2useStateComposition() {
  console.log('App: render')
  const [count, setCount] = useState<number>(0);
  const [random, setRandom] = useState<number>(0);

  return (
    <div className="comp">
      <h1>Demo Hooks: useState</h1>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setRandom(Math.random())}>Random</button>
      <Dashboard count={count} random={random}>
        <CounterPanel value={count} />
        <RandomPanel value={random} />
        <Buttons increment={() => setCount(count + 1)} />
      </Dashboard>
    </div>
  );
}

// Middle Component
// BAD: always rendered when state changes since it receives them as props

interface DashboardProps {
  count: number, random: number
}

const Dashboard: React.FC<DashboardProps> = React.memo(props => {
  console.log('Dashboard: render')
  return <div className="comp">
    Dashboard
    {props.children}
  </div>
})

// Child Component
// BAD: rendered even when Random is updated and it's not necessary
function CounterPanel (props: { value: number }) {
  console.log('CounterPanel: render')
  return <div className="comp">
    CounterPanel: {props.value}
  </div>
}

// Child Component
// BAD: rendered even when Count is updated and it's not necessary
// FIXED: use React.memo to memoize it
const RandomPanel: React.FC<{ value: number }> = React.memo((props) => {
  console.log('Random Panel: render')
  return <div className="comp">
    RandomPanel: {props.value}
  </div>
})


const Buttons: React.FC<{ increment: () => void }> = React.memo((props) => {
  console.log('Buttons: render')
  return <div className="comp">
    <button onClick={props.increment}>Update Counter</button>
  </div>
})
