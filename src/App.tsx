import React, { useState } from 'react';
import Demo1AuseState from './1.hooks/Demo1AuseState';
import Demo2useStateComposition from './1.hooks/Demo2useStateComposition';
import Demo3useReducer from './1.hooks/Demo3useReducer';
import Demo2AContext from './2.context/Demo2AContext';
import Demo2BContext from './2.context/Demo2BContext';
import Demo2CContext from './2.context/Demo2CContext';
import Demo2DContext from './2.context/Demo2DContext';
import Demo2EContextUseReducer from './2.context/Demo2EContextUseReducer';
import Demo3Recoil from './3.recoil/Demo3Recoil';
import Demo4ReduxToolkit from './4.redux-toolkit/Demo4ReduxToolkit';


function App() {
  return (
    <div>
      {/*<Demo1AuseState />*/}
      <Demo2useStateComposition />
      {/*<Demo3useReducer />*/}
      {/*<Demo2AContext />*/}
      {/*<Demo2BContext />*/}
      {/*<Demo2CContext />*/}
      {/*<Demo2DContext />*/}
      {/*<Demo2EContextUseReducer />*/}
      {/*{ <Demo3Recoil /> }*/}
      {/*<Demo4ReduxToolkit />*/}
    </div>
  );
}
export default App;

