import React, { useContext, useState } from 'react';

/**
 * Better use custom hooks for context
 * No better performance but:
 * PRO: you can globally handle errors in a unique point
 */

interface Count {
  value: number | null,
}

interface Random {
  value: number | null,
}

const CountContext = React.createContext<Count | undefined>(undefined);
const RandomContext = React.createContext<Random | undefined>(undefined);

export default function Demo2DContext() {
  return (
      <div className="comp">
        <h1>Demo D: Context</h1>
        <Dashboard  />
      </div>
  );
}

// Middle Component
const Dashboard = React.memo(() => {
  console.log('Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanelContainer />
    <RandomContainer />
  </div>
})

// Child Component
// NOTE: we don't wrap this part of the app with context, so it generates an error
function CounterPanelContainer () {
  const [count, setCount] = useState<number>(0);

  console.log('PanelCounter: render')
  return  (
    <div className="comp">
      CounterPanelContainer<br />
      {/*<CountContext.Provider value={{ value: count }}>*/}
        <button onClick={() => setCount(count - 1)}>-</button>
        <button onClick={() => setCount(count + 1)}>+</button>
        <CounterChild />
      {/*</CountContext.Provider>*/}
    </div>
  )
}

function CounterChild() {
  console.log('CounterPanel Child: render')
  const state = useContext(CountContext);
  if (state === undefined) {
    console.error('CounterPanel should be wrapped by CountContext ')
  }

  return <div className="comp">
    CounterChild: {state?.value} <br/>
  </div>
}

// Child Component
// NOTE: we don't wrap this part of the app with context, so it generates an error
function RandomContainer () {
  const [random, setRandom] = useState<number>(0);

  console.log('PanelAnother: render')
  return (
    <div className="comp">
      RandomContainer <br/>
      {/*<RandomContext.Provider value={{ value: random }}>*/}
        <button onClick={(a) => setRandom(Math.random())}>random</button>
        <AnotherChild />
      {/*</RandomContext.Provider>*/}
    </div>
  )
}

// useRandomContext hook
// PRO: unique location to define context behaviors
function useRandomContext() {
  const context = React.useContext(RandomContext)
  console.log(context)
  if (context === undefined) {
    console.error('useRandomContext should be used in componends wrapped by RandomContext ')
  }
  return context
}

// This component uses the custom hook
function AnotherChild() {
  const state = useRandomContext();
  return <div className="comp">
    Random Value: {state?.value}
  </div>
}
