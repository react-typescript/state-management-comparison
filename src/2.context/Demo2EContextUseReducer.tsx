import React, {  useContext, useReducer } from 'react';

/**
 * State management with useReducer
 * BAD: Children components are always updated when context is updated
 */

interface Counter {
  count: number;
  random: number;
}

type CounterActions = {
  type: string;
  payload?: Counter;
}

function countReducer(state: Counter, action: CounterActions) {
  switch (action.type) {
    case 'decrement': return {...state, count: state.count - 1};
    case 'increment': return {...state, count: state.count + 1};
    case 'random': return {...state, random: Math.random()};
    // default: throw new Error(`Unhandled action: ${action.type}`)
    default: return state;
  }
}

type Action = { type: 'increment' } | { type: 'decrement' } | { type: 'random'}
type Dispatch = (acion: Action) => void;

const CountStateContext = React.createContext<Counter | undefined>( undefined)
// const CountDispatchContext = React.createContext<Dispatch | undefined>(undefined)
const CountDispatchContext = React.createContext<Dispatch>(() => null);

/**
 * Main Smart Component
 * BAD: always rendered when state changes
 */
const initialState = { count: 1, random: 0};
export default function Demo2EContextUseReducer() {
  console.log('App: render')
  const [state, dispatch] = useReducer(countReducer, initialState);

  return (
    <div className="comp">
      <CountStateContext.Provider value={state}>
        <CountDispatchContext.Provider value={dispatch}>
            <h1>Demo Hooks: Context & useReducer</h1>
            <Dashboard />
        </CountDispatchContext.Provider>
      </CountStateContext.Provider>
    </div>
  );
}

// Middle Component
// BAD: always rendered when state changes since it receives them as props
// FIXED: by memoization
const Dashboard: React.FC = React.memo(() => {
  console.log('Dashboard: render')
  return <div className="comp">
    Dashboard
    <Buttons />
    <CounterPanel />
    <ButtonPanel />
  </div>
})

// Child Component
// GOOD: not rendered when another component update context
function Buttons () {
  console.log('Buttons: render')
  const dispatch = useContext(CountDispatchContext);
  // Needed because of TypeScript strict option, since context is not initialized
  if (!dispatch) { throw new Error('CountDispatchContext must be initialized and used within a CountDispatchContext Provider') }

  return <div className="comp">
    Buttons <br/>
    <button onClick={() => dispatch({ type: 'decrement' })}>-</button>
    <button onClick={() => dispatch({ type: 'increment' })}>+</button>
    <button onClick={() => dispatch({ type: 'random' })}>random</button>
  </div>;
}

// Child Component: consume / read context
// Behavior: Always rendered when context change
function CounterPanel () {
  console.log('CounterPanel: render')
  const state = useContext(CountStateContext);
  const dispatch = useContext(CountDispatchContext);

  return <div className="comp">
    CountPanel: {state?.count} - {state?.random}
    <button onClick={() => dispatch({ type: 'increment'})}>Increment (from child)</button>
  </div>
}

// Child Component: produce / dispatch
// BAD: always rendered when context changes since it receives them as props
// FIXED: by memoization
const ButtonPanel: React.FC = React.memo(() => {
  console.log('Button Panel: render')
  const dispatch = useContext(CountDispatchContext);

  return <div className="comp">
    button
    <button onClick={() => dispatch({ type: 'increment'})}>Increment (from child)</button>
  </div>
})


