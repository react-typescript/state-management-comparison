import React, { useContext, useState } from 'react';

/**
 * Each context handle a part of an application
 * PRO: no unnecessary re-renders.
 * PRO: Children are updated only when its context is updated
 */

interface Count {
  value: number | null,
}

interface Random {
  value: number | null,
}

const CountContext = React.createContext<Count>({ value: null });
const RandomContext = React.createContext<Random>({ value: null });

export default function Demo2CContext() {
  return (
      <div className="comp">
        <h1>Demo C: Context</h1>
        <Dashboard  />
      </div>
  );
}

// Middle Component
const Dashboard = React.memo(() => {
  console.log('Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanelContainer />
    <RandomPanelContainer />
  </div>
})

// Child Component
// GOOD: This is re-rendered even if counter is updated
function CounterPanelContainer () {
  const [count, setCount] = useState<number>(0);

  console.log('CounterPanelContainer: render')
  return  (
    <div className="comp">
      CounterPanelContainer: Context 1<br/>
      <CountContext.Provider value={{ value: count }}>
        <button onClick={() => setCount(count - 1)}>-</button>
        <button onClick={() => setCount(count + 1)}>+</button>
        <CounterChild />
      </CountContext.Provider>
    </div>
  )
}
// GOOD: This is re-rendered even if counter is updated
function CounterChild() {
  console.log('CounterPanel Child: render')

  const count = useContext(CountContext);
  return (<div className="comp">
    counter: {count.value} <br/>
  </div>)
}

// Child Component
// GOOD: This is re-rendered even if random is updated
function RandomPanelContainer () {
  console.log('RandomPanelContainer: render')
  const [random, setRandom] = useState<number>(0);

  return(
    <div className="comp">
      RandomPanelContainer: Context 2 <br/>
      <RandomContext.Provider value={{ value: random }}>
        <button onClick={(a) => setRandom(Math.random())}>random</button>
        <RandomPanelChild />
      </RandomContext.Provider>
    </div>
  )
}

// GOOD: This is re-rendered even if random is updated
function RandomPanelChild() {
  console.log('RandomPanel Child: render')
  const state = useContext(RandomContext);
  return (<div className="comp">
    Random Value: {state.value} <br/>
  </div>)
}
