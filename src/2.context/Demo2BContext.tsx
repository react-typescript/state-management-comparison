import React, { memo, useContext, useState } from 'react';

/**
 * Nested context example
 * UPDATE FROM PREVIOUS EXAMPLE: we use 2 different contexts instead one and they are nested
 * BAD: children components that use inner context is re-rendered
 * when the outer context is updated even if it's not necessary
 */

interface Count {
  value: number | null,
}

interface Random {
  value: number | null,
}

const CountContext = React.createContext<Count>({ value: null });
const RandomContext = React.createContext<Random>({ value: null });


/**
 * Main Smart Component
 * BAD: always rendered when Context changes since it updates a local state
 */
export default function Demo2BContext() {
  console.log('App: render')

  const [count, setCount] = useState<number>(0);
  const [random, setRandom] = useState<number>(0);

  return (
    <CountContext.Provider value={{ value: count }}>
      <RandomContext.Provider value={{ value: random }}>
        <h1>Demo B: Context</h1>
        <button onClick={() => setCount(count - 1)}>-</button>
        <button onClick={() => setCount(count + 1)}>+</button>
        <button onClick={() => setRandom(Math.random())}>Random</button>
        <Dashboard  />
      </RandomContext.Provider>
    </CountContext.Provider>
  );
}

// Middle Component
// Never re-rendered because of memoization
const Dashboard = React.memo(() => {
  console.log('dashboard: render')
  return <div className="comp">
    <CounterPanel />
    <RandomPanelMemo />
  </div>
})

// Child Component
// BAD: Updated when any value of Context change: count / random or another (although memoized)
const CounterPanel = React.memo(() => {
  const state = useContext(CountContext);

  console.log('CounterPanel: render')
  return <div className="comp">
    CounterPanel: {state.value}
  </div>
})


// Child Component
// This is re-rendered even if counter is updated and todos is not
function RandomPanel () {
  const state = useContext(RandomContext);

  console.log('RandomPanel: render')
  return <div className="comp">
    RandomPanel: {state.value}
  </div>
}

const RandomPanelMemo = memo(RandomPanel)
