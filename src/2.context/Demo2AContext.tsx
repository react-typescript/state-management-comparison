import React, { useContext, useState } from 'react';
/**
 * Context Usage
 * BAD: Children components are updated when the context value changes even if
 * they don't use that part of the state
 */


interface State {
  count: number | null,
  random: number | null
}

const AppContext = React.createContext<State>({ count: null, random: null });

/**
 * Main Smart Component
 * BAD: always rendered when Context changes since it updates a local state
 */
export default function Demo2AContext() {
  console.log('App: render')
  const [count, setCount] = useState<number>(1);
  const [random, setRandom] = useState<number>(0);

  return (
    <AppContext.Provider value={{ count, random }}>
      <h1>Demo A: Context</h1>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setRandom(Math.random())}>Random</button>
      <Dashboard  />
    </AppContext.Provider>
  );
}

// Middle Component
// BAD: rendered always since its parent is updated
// FIX: Rendered only first time since it's memoized (React.memo)
const Dashboard = React.memo(() => {
  console.log('dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel />
    <RandomPanel />
    <RandomPanelMemo />
  </div>
})

// Child Component
// BAD: Updated when any value of Context change: count or random
function CounterPanel () {
  const state = useContext(AppContext);

  console.log('CounterPanel: render')
  return <div className="comp">
    CounterPanel: {state.count}
  </div>
}


// Child Component
// BAD:  Updated when any value of Context change: count or random
function RandomPanel () {
  const state = useContext(AppContext);

  console.log('RandomPanel: render')
  return <div className="comp">
    RandomPanel: {state.random}
  </div>
}

// As RandomPanel, rendered even if counter is updated although if memoized
const RandomPanelMemo = React.memo(RandomPanel)

